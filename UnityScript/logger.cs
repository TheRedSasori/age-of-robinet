using System.Collections;
using UnityEngine;
using MongoDB.Bson;
using MongoDB.Driver;

public class loggerScript : MonoBehaviour
{
    string connectionString; // La chaine de caractères contenant l'hôte
    MongoClient client; // Déclaration du client mongo
    MongoServer server; // Déclaration du serveur mongo
    static MongoDatabase database; // Déclaration de la bdd mongo

    // Start is called before the first frame update
    void Start()
    {
        connectionString = "mongodb://127.0.0.1:8000/"; // L'hôte sur une base en local
        client = new MongoClient(connectionString); // Connexion

        server = client.GetServer(); // Récupération du serveur
        database = server.GetDatabase("AgeOfRobinet"); // On sélectionne la bdd (USE AgeOfRobinet)



        StartCoroutine(GetAllPlayerCoordinatesInDB()); // Démarrage du thread créé ci-dessous
    }


    IEnumerator GetAllPlayerCoordinatesInDB()
    {
        yield return new WaitForSecondsRealtime(3); // Attend 3 secondes
        string stringToSend = ""; // Déclaration de la chaine de caractère contenant les infos à envoyer (les coordonnées)
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player"); // Création d'une liste contenant tous les joueurs présents sur la scène du jeu (taggé "Player")
        foreach (GameObject player in players) // Pour chacun des joueurs
        {
            stringToSend += "&x=" + player.transform.position.x + ",z=" + player.transform.position.z; // On concatène un morceau de string en plus sous la forme "&x=0.00,z=0.00"
            // On ne récupère que X & Z sachant que les personnages ne sautent pas
        }
        //Debug.Log(stringToSend); // [debug] Affiche la chaine complète

        // Insertion du document bson, équivalent à INSERT INTO Game_History (id, duration, opponent_id, ..) VALUES("..","..","..",..);
        database.GetCollection<BsonDocument>("Game_History").Insert(new BsonDocument{
            { "id", 0 },
            { "duration", 0 },
            { "oppenent_id", 0 },
            { "winner", "None" },
            { "Game_id",  0},
            { "User1_id", 0 },
            { "User2_id", 0 },
            { "score", 0 },
            { "entities1_alive", 0 },
            { "entities2_alive", 0 },
            { "entities1_killed", 0 },
            { "entities2_killed", 0 },
            { "everyplayercoord", stringToSend }, // Ici on insère les données stockées dans le string
            { "current_money", 0 },
            { "earned_money", 0 },
            { "built_buildings", 0 },
            { "current_buildings", 0 }
        });
        Debug.Log("Document inséré !");

        StartCoroutine(GetAllPlayerCoordinatesInDB()); // On relance le thread de manière imbriqué
    }
}
